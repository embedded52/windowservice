﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Net.NetworkInformation;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
namespace serviceCheckUSB
{
    public partial class Service1 : ServiceBase
    {
        Timer timer = new Timer();
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            WriteToFile("Service is started at " + DateTime.Now);
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Interval = 2000; //number in milisecinds
            timer.Enabled = true;
            //
        }

        protected override void OnStop()
        {
            WriteToFile("Service is stopped at " + DateTime.Now);
        }

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            //printer
            //GetPrinterDevices();

            //disks
            //GetDiskDevices();

            //usb
            if(true == GetUSBDevices())
            {
                WriteToFile("shut down");
                Shutdown();
            }

            //comport
            if (true == GetUSBComport())
            {
                WriteToFile("shut down");
                Shutdown();
            }

            //network
            if (true == GetNetworkAdapter())
            {
                WriteToFile("shut down");
                Shutdown();
            }

            //all driver
            //GetALlWin32_PnPSignedDriver();
        }
        public static void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }
        static int GetALlWin32_PnPSignedDriver()
        {
            int num = 0;
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_PnPSignedDriver");
            num = searcher.Get().Count;
            WriteToFile("num all drivers= " + num);

            foreach (ManagementObject obj in searcher.Get())
            {
                string s1, s2, s3, s4, s5, s6, s7, s8, s9, s10;
                //Device name    
                s1 = string.IsNullOrEmpty(obj.GetPropertyValue("DeviceName").ToString()) ? string.Empty : obj.GetPropertyValue("DeviceName").ToString();
                WriteToFile(s1);
                s2 = string.IsNullOrEmpty(obj.GetPropertyValue("Description").ToString()) ? string.Empty : obj.GetPropertyValue("Description").ToString();
                WriteToFile(s2);
                s3 = string.IsNullOrEmpty(obj.GetPropertyValue("DeviceID").ToString()) ? string.Empty : obj.GetPropertyValue("DeviceID").ToString();
                WriteToFile(s3);
                //s4 = string.IsNullOrEmpty(obj.GetPropertyValue("DriverName").ToString()) ? string.Empty : obj.GetPropertyValue("DriverName").ToString();
                //WriteToFile(s4);
                s5 = string.IsNullOrEmpty(obj.GetPropertyValue("DriverVersion").ToString()) ? string.Empty : obj.GetPropertyValue("DriverVersion").ToString();
                WriteToFile(s5);
                //s6 = string.IsNullOrEmpty(obj.GetPropertyValue("Name").ToString()) ? string.Empty : obj.GetPropertyValue("Name").ToString();
                //WriteToFile(s6);
                //s8 = string.IsNullOrEmpty(obj.GetPropertyValue("SystemName").ToString()) ? string.Empty : obj.GetPropertyValue("SystemName").ToString();
                //WriteToFile(s8);
                //s9 = string.IsNullOrEmpty(obj.GetPropertyValue("InstallDate").ToString()) ? string.Empty : obj.GetPropertyValue("InstallDate").ToString();
                //WriteToFile(s9);
                s10 = string.IsNullOrEmpty(obj.GetPropertyValue("Manufacturer").ToString()) ? string.Empty : obj.GetPropertyValue("Manufacturer").ToString();
                WriteToFile(s10);
            }
            return num;
        }

        static bool GetNetworkAdapter()
        {
            int num = 0;
            ManagementObjectSearcher networkAdapterSearcher = new ManagementObjectSearcher("root\\cimv2", "select * from Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection objectCollection = networkAdapterSearcher.Get();
            num = objectCollection.Count;

            num = NetworkInterface.GetAllNetworkInterfaces().Length;
            WriteToFile("num network interface= " + num);

            //foreach (ManagementObject networkAdapter in objectCollection)
            //{
            //    PropertyDataCollection networkAdapterProperties = networkAdapter.Properties;
            //    WriteToFile("description : " + networkAdapter.GetPropertyValue("Description") + ",  caption: " + networkAdapter.GetPropertyValue("Caption"));


            //    foreach (PropertyData networkAdapterProperty in networkAdapterProperties)
            //    {
            //        if (networkAdapterProperty.Value != null)
            //        {
            //            WriteToFile("name : " + networkAdapterProperty.Name + ",  value: " + networkAdapterProperty.Value);
            //        }
            //    }
            //}
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                //values.Add(nic.Name);
                WriteToFile("name : " + nic.Name + ",  des: " + nic.Description + ", id: " + nic.Id);
                if (nic.Name.Contains("Wi-Fi"))
                {
                    WriteToFile("detected new network");
                    return true;
                }
            }

            return false;
        }

        static int GetDiskDevices()
        {
            int num = 0;
            DriveInfo[] allDrives = DriveInfo.GetDrives();
            num = allDrives.Length;
            WriteToFile("num disks= " + num);
            foreach (DriveInfo d in allDrives)
            {
                WriteToFile("name drive: " + d.Name + ", drive type: " + d.DriveType);
            }
            return num;
        }
        static int GetPrinterDevices()
        {
            int num = 0;
            //WriteToFile("printers= "+System.Drawing.Printing.PrinterSettings.InstalledPrinters.Count.ToString());
            //foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            //{
            //    WriteToFile(printer);
            //}
            var printerQuery = new ManagementObjectSearcher("SELECT * from Win32_Printer");
            num = printerQuery.Get().Count;
            WriteToFile("num printers= "+ num);
            foreach (var printer in printerQuery.Get())
            {
                var name = printer.GetPropertyValue("Name");
                var status = printer.GetPropertyValue("Status");
                var isDefault = printer.GetPropertyValue("Default");
                var isNetworkPrinter = printer.GetPropertyValue("Network");
                WriteToFile("name: " + name + ", status: " + status + ", default: " + isDefault + ", network: " + isNetworkPrinter);
            }
            return num;
        }

        static bool GetUSBDevices()
        {
            int num = 0;
            ManagementObjectCollection collection;
            using (var searcher = new ManagementObjectSearcher(@"Select * From Win32_USBHub"))
                collection = searcher.Get();
            num = collection.Count;
            WriteToFile("num usbs= " + num);
            foreach (var device in collection)
            {
                WriteToFile("DeviceID: " + device.GetPropertyValue("DeviceID") + " PNPDeviceID: " + device.GetPropertyValue("PNPDeviceID") + " Description: " + device.GetPropertyValue("Description"));
                if (device.GetPropertyValue("Description").ToString().Contains("Printing"))
                {
                    WriteToFile("detect a usb printing support");
                    num--;
                }
            }
            collection.Dispose();
            WriteToFile("num real usbs= " + num);
            if (num > 2)
            {
                WriteToFile("detect new usb");
                return true;
            }
            return false;
        }

        static bool GetUSBComport()
        {
            int num = 0;

            try
            {
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\WMI",
                    "SELECT * FROM MSSerial_PortName");
                num = searcher.Get().Count;
                WriteToFile("num comports= " + num);
                foreach (ManagementObject queryObj in searcher.Get())
                {
                    WriteToFile("InstanceName= " + queryObj["InstanceName"] + " PortName= "+ queryObj["PortName"]);
                }
            }
            catch (ManagementException e)
            {
                WriteToFile("An error occurred while querying for WMI data: " + e.Message);
            }
            if(num > 1)
            {
                WriteToFile("detect new comport");
                return true;
            }

            return false;
        }

        void Shutdown()
        {
            Process.Start("shutdown.exe", "-s -t 00");
        }
    }
}
