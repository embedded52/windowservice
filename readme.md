step to add a service to window:
install:
1. open cmd as admin -> cd C:\Windows\Microsoft.NET\Framework\v4.0.30319 
2. InstallUtil.exe + Your copied path + \your service name + .exe

uninstall:
1. open cmd as admin cd C:\Windows\Microsoft.NET\Framework\v4.0.30319 
2. InstallUtil.exe -u + Your copied path + \your service name + .exe

NOTE: config run service and auto startup service
1. Windows + R -> services.msc
2. Double click in my service: MyService.Demo
3. Set startup type: Automatic -> apply.
4. click Run service.